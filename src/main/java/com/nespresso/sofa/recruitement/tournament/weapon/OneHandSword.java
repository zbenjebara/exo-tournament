package com.nespresso.sofa.recruitement.tournament.weapon;

import com.nespresso.sofa.recruitement.tournament.Damage;

public class OneHandSword extends Weapon {

    public OneHandSword(Damage penaltyDamage) {
        super(penaltyDamage);
    }

    @Override
    public Damage weaponDamage() {
        return Damage.penalty(new Damage(5), penaltyDamage);
    }
}
