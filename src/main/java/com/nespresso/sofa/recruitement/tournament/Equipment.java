package com.nespresso.sofa.recruitement.tournament;

import com.nespresso.sofa.recruitement.tournament.weapon.Weapon;

public interface Equipment {
    Damage absorbWeaponAttack(final Weapon weapon);
}
