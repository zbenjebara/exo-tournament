package com.nespresso.sofa.recruitement.tournament;

import com.nespresso.sofa.recruitement.tournament.weapon.Axe;
import com.nespresso.sofa.recruitement.tournament.weapon.Weapon;

public class Viking extends AbstractFighter {

    private Weapon weapon = new Axe(Damage.EMPTY_DAMAGE);

    public Viking() {
        super(120);
    }

    @Override
    protected Damage damage() {
        return new Damage(6);
    }

    @Override
    protected Weapon weapon() {
        return weapon;
    }
}
