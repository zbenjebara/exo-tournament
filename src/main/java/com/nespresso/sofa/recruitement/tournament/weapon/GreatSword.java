package com.nespresso.sofa.recruitement.tournament.weapon;

import com.nespresso.sofa.recruitement.tournament.Damage;

public class GreatSword extends Weapon {

    private int attackCounter = 0;

    public GreatSword(Damage penaltyDamage) {
        super(penaltyDamage);
    }

    @Override
    public Damage weaponDamage() {
        if (attackCounter == 3) {
            attackCounter = 0;
            return Damage.EMPTY_DAMAGE;
        }
        return Damage.penalty(new Damage(12), penaltyDamage);
    }
}
