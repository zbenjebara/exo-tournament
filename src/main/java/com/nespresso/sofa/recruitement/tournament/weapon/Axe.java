package com.nespresso.sofa.recruitement.tournament.weapon;

import com.nespresso.sofa.recruitement.tournament.Damage;

public class Axe extends Weapon {

    public Axe(Damage penaltyDamage) {
        super(penaltyDamage);
    }

    @Override
    public Damage weaponDamage() {
        return Damage.penalty(new Damage(6), penaltyDamage);
    }
}
