package com.nespresso.sofa.recruitement.tournament;

import com.nespresso.sofa.recruitement.tournament.weapon.Axe;
import com.nespresso.sofa.recruitement.tournament.weapon.Weapon;

public class Buckler implements Equipment {

    enum Stat {
        DESTOYED, FINE
    }

    private boolean cancelAttack = false;

    private Stat stat = Stat.FINE;

    private int hardAttackBlockingCounter = 0;

    @Override
    public Damage absorbWeaponAttack(final Weapon weapon) {
        if (cancelAttack || stat == Stat.DESTOYED) {
            cancelAttack = false;
            return weapon.weaponDamage();
        }
        return absorbAttack(weapon);
    }

    private Damage absorbAttack(final Weapon weapon) {
        if (weapon instanceof Axe)
            hardAttackBlockingCounter++;
        if (hardAttackBlockingCounter == 3)
            stat = Stat.DESTOYED;
        cancelAttack = true;
        return Damage.EMPTY_DAMAGE;
    }
}
