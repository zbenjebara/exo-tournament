package com.nespresso.sofa.recruitement.tournament;

import com.nespresso.sofa.recruitement.tournament.weapon.GreatSword;
import com.nespresso.sofa.recruitement.tournament.weapon.Weapon;

public class Highlander extends AbstractFighter{

    public Highlander() {
        super(150);
    }

    public Highlander(String veteran) {
        super(100);
    }

    @Override
    protected Damage damage() {
        return null;
    }


    @Override
    protected Weapon weapon() {
        return new GreatSword(Damage.EMPTY_DAMAGE);
    }
}
