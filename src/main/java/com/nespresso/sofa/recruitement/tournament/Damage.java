package com.nespresso.sofa.recruitement.tournament;

public class Damage {

    public static final Damage EMPTY_DAMAGE = new Damage(0);

    private final int value;

    public Damage(int value) {
        this.value = value;
    }

    public static int absorb(Damage damage, int from) {
        final int damageAbsorbed = from - damage.value;
        return damageAbsorbed < 0 ? 0 : damageAbsorbed;
    }

    public Damage absorb(int absorbedValue) {
        final int newValue = value - absorbedValue;
        return newValue <= 0 ? EMPTY_DAMAGE : new Damage(newValue);
    }

    public static Damage penalty(Damage damage1, Damage damage2) {
        final int newValue = damage1.value - damage2.value;
        return newValue <= 0 ? EMPTY_DAMAGE : new Damage(newValue);
    }

    @Override
    public String toString() {
        return "Damage{" +
                "value=" + value +
                '}';
    }
}
