package com.nespresso.sofa.recruitement.tournament;

import com.nespresso.sofa.recruitement.tournament.weapon.OneHandSword;
import com.nespresso.sofa.recruitement.tournament.weapon.Weapon;

public class Swordsman extends AbstractFighter {

    private Weapon weapon = new OneHandSword(Damage.EMPTY_DAMAGE);

    public Swordsman() {
        super(100);
    }

    public Swordsman(String vicious) {
        super(100);
    }

    @Override
    protected Damage damage() {
        return new Damage(5);
    }

    @Override
    protected Weapon weapon() {
        final Damage penaltyDamage = penaltyDamage();
        if (penaltyDamage != Damage.EMPTY_DAMAGE)
            weapon = new OneHandSword(penaltyDamage);
        return weapon;
    }

    public Damage penaltyDamage() {
        Damage damage = Damage.EMPTY_DAMAGE;
        for (Equipment equipment : equipments) {
            if (equipment instanceof Armor)
                damage = new Damage(1);
        }
        return damage;
    }
}
