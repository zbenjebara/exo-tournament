package com.nespresso.sofa.recruitement.tournament;

public enum EquipmentFactory {
    INSTANCE;

    Equipment create(final String equipment) {
        if (equipment.equals("buckler"))
            return new Buckler();
        else if (equipment.equals("armor"))
            return new Armor();
        throw new IllegalArgumentException();
    }
}
