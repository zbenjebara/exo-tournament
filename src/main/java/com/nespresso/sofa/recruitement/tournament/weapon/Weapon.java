package com.nespresso.sofa.recruitement.tournament.weapon;

import com.nespresso.sofa.recruitement.tournament.Damage;

public abstract class Weapon {

    protected final Damage penaltyDamage;

    protected Weapon(Damage penaltyDamage) {
        this.penaltyDamage = penaltyDamage;
    }

    public abstract Damage weaponDamage();
}
