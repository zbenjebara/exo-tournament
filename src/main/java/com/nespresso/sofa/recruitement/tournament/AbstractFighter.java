package com.nespresso.sofa.recruitement.tournament;

import com.nespresso.sofa.recruitement.tournament.weapon.Weapon;

import java.util.LinkedList;
import java.util.List;

public abstract class AbstractFighter {

    protected int hitPoints;

    protected List<Equipment> equipments = new LinkedList<>();

    protected AbstractFighter(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public void engage(final AbstractFighter enemy) {
        do {
            enemy.absorbWeaponAttack(weapon());
            this.absorbWeaponAttack(enemy.weapon());
        } while (this.hitPoints > 0 && enemy.hitPoints > 0);
    }

    private void absorbWeaponAttack(final Weapon weapon) {
        Damage damage = weapon.weaponDamage();
        for (final Equipment equipment : equipments) {
            damage = equipment.absorbWeaponAttack(weapon);
        }
        hitPoints = Damage.absorb(damage, hitPoints);
    }

    protected abstract Damage damage();

    protected abstract Weapon weapon();

    public <T extends AbstractFighter> T equip(String equip) {
        equipments.add(EquipmentFactory.INSTANCE.create(equip));
        return (T) this;
    }

    public int hitPoints() {
        return (hitPoints < 0) ? 0 : hitPoints;
    }

}
