package com.nespresso.sofa.recruitement.tournament;

import com.nespresso.sofa.recruitement.tournament.weapon.Weapon;

public class Armor implements Equipment {

    @Override
    public Damage absorbWeaponAttack(final Weapon weapon) {
        return weapon.weaponDamage().absorb(3);
    }
}
